﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour {
    
    public float tileXWidth;
    public float tileZWidth;

    public int numtiles;

    public int numCols;
    public int numRows;

    public bool seeded;
    public int seed;

    public bool dayseed;


    public List<GameObject> tilePrefabs;

    // Use this for initialization
    void Start () {
        if (seeded == true) //awta thw manually selected seed
        {
            Random.seed = seed;
        }

        if (dayseed == true) {// sets the day as the seed
            Random.seed = System.DateTime.Now.Day;


        }


        transform.position = new Vector3(0, 0, 0);
        BuildMap();

    }
	
	// Update is called once per frame
	void Update () {
        

    }



    public void BuildMap() //builds the map
    {
        for (int currentRow = 0; currentRow < numRows; currentRow++) //goes through each row until the max 
        {
            for (int currentCol = 0; currentCol < numCols; currentCol++) //goes through each collum
            {
                // Generate a random tile
                int rand = Random.Range(0, numtiles);
                GameObject newTile = Instantiate(tilePrefabs[rand]) as GameObject;
                newTile.name = "Tile (" + currentCol + "," + currentRow + ")";
                // Make this tile a child of THIS object
                newTile.transform.parent = this.transform;

                // move it into position
                float XPosition = currentCol * tileXWidth;
                float ZPosition = currentRow * tileZWidth;
                newTile.transform.localPosition = new Vector3(XPosition, 0.0f, ZPosition);
            }
        }
    }


}

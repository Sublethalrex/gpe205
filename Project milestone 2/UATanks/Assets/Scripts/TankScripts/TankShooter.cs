﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankShooter : MonoBehaviour
{


    private CharacterController cc;
    [HideInInspector] public Transform tf;
    private TankData data;
    public GameObject bulletprefab;
    public Transform bulletSpawn;
    public float destroytime;

    // Use this for initialization

    void Start()
    {

    }

    // Update is called once per frame





    public void shoot()
    {
        // Create the Bullet from the Bullet Prefab
        var bullet = (GameObject)Instantiate(
            bulletprefab,
            bulletSpawn.position,
            bulletSpawn.rotation);

        // Add velocity to the bullet
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 6;

        // Destroy the bullet after a set amount of time
        Destroy(bullet, destroytime);
    }
   

}


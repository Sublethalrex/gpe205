﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TankMover))]
[RequireComponent(typeof(TankShooter))]

public class TankData : MonoBehaviour {

	public float moveSpeed;
    public float backmoveSpeed;
	public float turnSpeed;
    public float shotdelay;
    public float health;
    public float bulletdamage;

    // Other Tank Components
    [HideInInspector] public TankMover mover;
	[HideInInspector] public TankShooter shooter;

	void Start() {
		mover = GetComponent<TankMover> ();
		shooter = GetComponent<TankShooter> ();
	}

    void OnCollsionsEnter(Collision collision) { //detect if bullet hits, doesnt work

        if (collision.gameObject.tag == "Bullet") {

            health = health - bulletdamage; 
            Debug.Log("hello");
        }


        if (health <= 0)
        {

            Destroy(this);

        }

    }
    void oncollisionenter(Collision col) {

        if (col.gameObject.tag == "Speed") {
            moveSpeed = moveSpeed * 2;


        }

    }


}

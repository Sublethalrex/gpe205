﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{

    public TankData pawn;

    public KeyCode forwardKey = KeyCode.UpArrow;
    public KeyCode backKey = KeyCode.DownArrow;
    public KeyCode turnRightKey = KeyCode.RightArrow;
    public KeyCode turnLeftKey = KeyCode.LeftArrow;
    public KeyCode fire = KeyCode.Space;

    // Use this for initialization
    void Start()
    {
        GameManager.instance.player = this;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKey(fire))
        {
            pawn.shooter.shoot();
        }

        if (Input.GetKey(forwardKey))
        {
            pawn.mover.Move(pawn.mover.tf.forward);
        }
        if (Input.GetKey(backKey))
        {
            pawn.mover.Moveback(-pawn.mover.tf.forward);
        }
        if (Input.GetKey(turnRightKey))
        {
            pawn.mover.Turn(1);
        }
        if (Input.GetKey(turnLeftKey))
        {
            pawn.mover.Turn(-1);
        }
    }
}


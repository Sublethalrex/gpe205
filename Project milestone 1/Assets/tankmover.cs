﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tankmover : MonoBehaviour {

	private CharacterController cc;
	[HideInInspector]public Transform tf;
	private TankData data;

	// Use this for initialization
	void Start () {
		cc = GetComponent<CharacterController> ();
		tf = transform;
		data = GetComponent<TankData> ();


	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Move(Vector3 moveVector){
		cc.SimpleMove (moveVector);
	}


	public void Turn(float direction){
		tf.Rotate (0, direction * Time.deltaTime, 0);



	}



}

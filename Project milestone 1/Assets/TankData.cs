﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[RequireComponent(typeof(tankmover))]
[RequireComponent(typeof(tankshooter))]


public class TankData : MonoBehaviour {



	public float forwardspeed;
	public float reversespeed;
	public float turnspeed;
		

	[HideInInspector]public tankmover mover;
	[HideInInspector]public tankshooter shooter;

	void Start(){
	
		mover = GetComponent<tankmover> ();
		shooter = GetComponent<tankshooter> ();

		gamemanager.instance.player = this;
	
	}



	
}
